
function myFunction(val) 
{

    var price;
    var value;
    var prod = document.getElementById("product").value;
    var val = document.getElementById("quantity").value; 
    var discount = 0;
    if (prod == "Coca Cola") {
        price = 4.8;
        value = val * price;
 
    }
  
    if (prod == "Pepsi") {
        price = 5.8;
        value = val * price
    
        if (val >= 3) {
            
            discount = 0.20 * value ;
            value = value - discount;
        }
    }

    if (prod == "Sprite") {
        price = 3.8;  
        value = val * price;
    }

    if (prod == "Lemon") {
        price = 2.8;
        value = val * price;
    }

    if (prod == "Fanta") {
        price = 6.8;
        value = val * price;
    }

    document.getElementById("price").value =  price;
    document.getElementById("total").value =  value.toFixed(2);
    document.getElementById("result").innerHTML = value.toFixed(2)+" Eur "+discount+" discount";

}

function cancel()
{

    document.getElementById("price").value =  "";
    document.getElementById("total").value = "";
    document.getElementById("result").innerHTML ="";
    $('#inventory')[0].reset();
}

function delete_order(id)
{
    $.ajax
    ({
       type:'post',
       url:'<?php echo site_url('/welcome/delete'); ?>',
       data:{
           delete_row:'delete_row',
           row_id:id,
       },

       success:function(response) {
            if (response=="success") {
                var order=document.getElementById("order"+id);
                order.parentNode.removeChild(order);
            } else {
                alert ("Issue occured, retry later");
            }

       }
    });
}

function searchFilter(page_num) 
{
    page_num = page_num?page_num:0; 
    var search = document.getElementById("search").value;
    var filter = document.getElementById("filter").value;   
    $.ajax({
        type: 'POST',
        url: '<?php echo base_url(); ?>welcome/paginated/'+page_num,
        data:'page='+page_num+'&search='+search+'&filter='+filter,
        success: function (html) {    
            $('#history').html(html);        
        }
    });
}

function edit_order(id)
{
    var client=document.getElementById("client"+id).innerHTML;
    var product=document.getElementById("product"+id).innerHTML;
    var quantity=document.getElementById("quantity"+id).innerHTML;
    var price=document.getElementById("price"+id).innerHTML;
    var total=document.getElementById("total"+id).innerHTML;
    document.getElementById("client").value=client;
    document.getElementById("product").value=product;
    document.getElementById("quantity").value=quantity;
    document.getElementById("update").value=id;
    document.getElementById("price").value=price;
    document.getElementById("total").value=total;
    document.getElementById("submit").value="Update";
}

$(document).ready(function(){

    $('#submit').click(function(e){

        e.preventDefault();
        var client = $("#client").val();
        var quantity = $("#quantity").val();
        var product = $("#product").val();
        var price = $("#price").val();
        var total = $("#total").val();
        var update = $("#update").val();
        $.ajax({

            type: "POST",
            url:"<?php echo site_url('/welcome/validate'); ?>",
            data: $("#inventory").serialize(),
            success : function(data) {
                if (data == "-1"){
                alert("Db error, try again later");

              } else if (data == "-2") {
       
                  $("#error-client").html("Type required Joe Doe");
                  $("#error-qty").html("Integer required");                
                } else { 
                    if (!update == "") {
                        
                        var order=document.getElementById("order"+update);
                        order.parentNode.removeChild(order); 
                    }
                  var id= data;
                  var table=document.getElementById("order_table");
                  var table_len=(table.rows.length)-1;
                  var d = new Date();
                  var row = table.insertRow(table_len).outerHTML="<tr id='order"+id+"'><td id='client"+id+"'>"+client+"</td><td id='product"+id+"'>"+product+"</td><td id='price"+id+"'>"+price+" Eur </td><td id='quantity"+id+"'>"+quantity+"</td><td id='total"+id+"'>"+total+" Eur </td><td id='date"+id+"'>"+d.toUTCString()+"</td><td><input type='button'  id='edit_button"+id+"' value='edit' onclick='edit_order("+id+");'/><input type='button' id='delete_button"+id+"' value='delete' onclick='delete_order("+id+");'/></td></tr>";
                  $('#inventory')[0].reset();
                  document.getElementById("result").innerHTML="";
                }     
            }
        });
    });
});
