<?php
error_reporting(E_ALL);

class Inventory_model extends CI_Model
{
    public function _construct()
    {
        parent::_construct;
    }

    public function add_order($client, $quantity, $product,$id, $total, $price) 
    {
        
        $data = array('User'=>$client,
        'Product'=>$product,
        'Quantity'=>$quantity,
        'Price'=>$price,
        'Total'=>$total);

        if (empty($id)) {

            if ($this->db->insert('order', $data)) {

                $insertId = $this->db->insert_id();
                return $insertId;

            } else {

                return -1;
            }

        } else { 

            $this->db->where('id', $id);
            if ($this->db->update('order', $data)) {

                return $id;
            } else {

                return -1;
            }
        }
    }

    public function order_count() 
    {
        return $this->db->count_all('order');
    
    }

    public function getpaginated($params=array())
    {
        $date = date('Y-m-d H:i:s');

        if (($params['filter']) == 0) {

            $this->db->where('Date <=', $date);
        }

        if ($params['filter'] == 1) {

            $date = new DateTime("now");
            $curr_date = $date->format('Y-m-d');
            $this->db->where('DATE(Date)', $curr_date);
        }

        if ($params['filter'] == 2) {

            $lastweek = date('Y-m-d H:i:s', strtotime("-1 week"));
            $this->db->where('Date <=', $date);
            $this->db->where('Date >=', $lastweek);
        }

        if (!empty($params['search'])) {

            $this->db->like('Product', $params['search']);
            $this->db->or_like('User', $params['search']);
        }

        if (!empty($params['start']) && !empty($params['limit'])) {

            $this->db->limit($params['limit'], $params['start']);
        
        } elseif (empty($params['start']) && !empty($params['limit'])) {

            $this->db->limit($params['limit']);
        }
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('order');
        return ($query->num_rows() > 0)?$query->result_array():FALSE;
    }

    public function get_orders($limit, $start) 
    {
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get_where('order');
        return $query->result();
    }

    public function get_clients() 
    {
        
        $this->db->order_by('id', 'desc');
        $query = $this->db->get_where('user');
        return $query->result();
    }

    public function delete_order($order)
    {
        $this->db->where('id', $order);
        if ($this->db->delete('order')) {
            
            return 1;
        } else {

            return 0;
        }
    }
}