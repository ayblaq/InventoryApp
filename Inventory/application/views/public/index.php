<!DOCTYPE html>
<html>
<head>
	<title>Inventory Application</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<?php echo $css; ?>
</head>
<body>
<h3>Welcome to the inventory App</h3>
<div class="formcontainer">
  <form role="form" name="inventory" action="#" id="inventory" >

    <label for="cname">Client</label> 
    <select id="client" name="client" >
    <?php if (isset($clients)) { foreach ($clients as $row) { ?>
      <option value="<?php echo $row->Name; ?>"><?php echo $row->Name; } }?></option>

    </select>

     <label for="product">Product</label>
    <select id="product" name="product" onchange="myFunction(this.value)">
      <option value="Fanta">Fanta (6.8 Eur)</option>
      <option value="Coca Cola">Coca Cola (4.8 Eur)</option>
      <option value="Pepsi">Pepsi  (5.8 Eur)</option>
      <option value="Sprite">Sprite (3.8 Eur)</option>
      <option value="Lemon">Lemon (2.8 Eur)</option>
    </select>

    <label for="quantity">Quantity</label> <p id="error-qty" class="error"></p> <p id="result" class="result"></p>
    <input type="text" id="quantity" name="quantity" placeholder="Quantity.." onkeyup="myFunction(this.value)">
    <input type="hidden" id="price" name="price" value="" >
    <input type="hidden" id="total" name="total" value="" >
    <input type="hidden" id="update" name="update" value="">
    <span></span>
  

    <input type="submit" class="button" id=submit value="Add Order" >
    <input type="submit" class="button" id=submit value="cancel" onclick="cancel()" >

  </form>
<br/><br/>

<select id="filter" name="filter" onchange="searchFilter()">
      <option value="0">All Time</option>
      <option value="2">Last 7 days</option>
      <option value="1">Today</option>
    </select> <input type="text" id="search" onkeyup="searchFilter()" placeholder="Search for names..">

  <div style="overflow-x:auto;" id="history">
  <table style="width:100%" id="order_table">
  <caption>Orders</caption>
  <tr>
    <th>User</th>
    <th>Product</th>
    <th>Price</th>
    <th>Quantity</th>
    <th>Total</th>
    <th>Date</th>
    <th>Action</th>
  </tr>
 <?php if(isset($orders)) { foreach ($orders as $row) { ?>
  <tr id="order<?php echo $row->id;?>">
  <td id="client<?php echo $row->id;?>"><?php echo $row->User;?></td>
  <td id="product<?php echo $row->id;?>"><?php echo $row->Product;?></td>
  <td id="price<?php echo $row->id;?>"><?php echo $row->Price." EUR";?></td>
  <td id="quantity<?php echo $row->id;?>"><?php echo $row->Quantity;?></td>
  <td id="total<?php echo $row->id;?>"><?php echo $row->Total." EUR";?></td>
  <td id="date<?php echo $row->id;?>"><?php echo $row->Date;?></td>
  <td>
   <input type='button'  id="edit_button<?php echo $row->id;?>" value="edit" onclick="edit_order('<?php echo $row->id;?>');"> 
   <input type='button'  id="delete_button<?php echo $row->id;?>" value="delete" onclick="delete_order('<?php echo $row->id;?>');">
  </td>
  </tr>
 <?php } } ?>
</table>
<br/>
<?php echo $this->ajax_pagination->create_links() ?>
</div>

            
</div>

</body>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script>

function myFunction(val) 
{

    var price;
    var value;
    var prod = document.getElementById("product").value;
    var val = document.getElementById("quantity").value; 
    if (val % 1 === 0) {

        var discount = 0;
        if (prod == "Coca Cola") {
            price = 4.8;
            value = val * price;
 
        }
  
        if (prod == "Pepsi") {
            price = 5.8;
            value = val * price
    
            if (val >= 3) {
            
                discount = 0.20 * value ;
                value = value - discount;
            }
        }

        if (prod == "Sprite") {
            price = 3.8;  
            value = val * price;
        }

        if (prod == "Lemon") {
            price = 2.8;
            value = val * price;
        }

        if (prod == "Fanta") {
            price = 6.8;
            value = val * price;
        }

        document.getElementById("price").value =  price;
        document.getElementById("total").value =  value.toFixed(2);
        document.getElementById("result").innerHTML = value.toFixed(2)+" Eur "+discount+" discount";
    }else {
        document.getElementById("result").innerHTML = "Quantity should be an integer";
    } 
}

function cancel()
{

    document.getElementById("price").value =  "";
    document.getElementById("total").value = "";
    document.getElementById("result").innerHTML ="";
    $('#inventory')[0].reset();
}

function delete_order(id)
{
    $.ajax
    ({
       type:'post',
       url:'<?php echo site_url('/inventorycontroller/delete'); ?>',
       data:{
           delete_row:'delete_row',
           row_id:id,
       },

       success:function(response) {
            if (response=="success") {
                var order=document.getElementById("order"+id);
                order.parentNode.removeChild(order);
            } else {
                alert ("Issue occured, retry later");
            }

       }
    });
}

function searchFilter(page_num) 
{
    page_num = page_num?page_num:0; 
    var search = document.getElementById("search").value;
    var filter = document.getElementById("filter").value;   
    $.ajax({
        type: 'POST',
        url: '<?php echo base_url(); ?>inventorycontroller/paginated/'+page_num,
        data:'page='+page_num+'&search='+search+'&filter='+filter,
        success: function (html) {    
            $('#history').html(html);        
        }
    });
}

function edit_order(id)
{
    var client=document.getElementById("client"+id).innerHTML;
    var product=document.getElementById("product"+id).innerHTML;
    var quantity=document.getElementById("quantity"+id).innerHTML;
    var price=document.getElementById("price"+id).innerHTML;
    var total=document.getElementById("total"+id).innerHTML;
    document.getElementById("client").value=client;
    document.getElementById("product").value=product;
    document.getElementById("quantity").value=quantity;
    document.getElementById("update").value=id;
    document.getElementById("price").value=price;
    document.getElementById("total").value=total;
    document.getElementById("submit").value="Update";
}

$(document).ready(function(){

    $('#submit').click(function(e){

        e.preventDefault();
        var client = $("#client").val();
        var quantity = $("#quantity").val();
        var product = $("#product").val();
        var price = $("#price").val();
        var total = $("#total").val();
        var update = $("#update").val();
        $.ajax({

            type: "POST",
            url:"<?php echo site_url('/inventorycontroller/validate'); ?>",
            data: $("#inventory").serialize(),
            success : function(data) {
                if (data == "-1"){
                alert("Db error, try again later");

              } else if (data == "-2") {
       
                 
                  $("#error-qty").html("Integer required");                
                } else { 
                    if (!update == "") {
                        
                        var order=document.getElementById("order"+update);
                        order.parentNode.removeChild(order); 
                    }
                  var id= data;
                  var table=document.getElementById("order_table");
                  var table_len=(table.rows.length)-1;
                  var d = new Date();
                  var row = table.insertRow(table_len).outerHTML="<tr id='order"+id+"'><td id='client"+id+"'>"+client+"</td><td id='product"+id+"'>"+product+"</td><td id='price"+id+"'>"+price+" Eur </td><td id='quantity"+id+"'>"+quantity+"</td><td id='total"+id+"'>"+total+" Eur </td><td id='date"+id+"'>"+d.toUTCString()+"</td><td><input type='button'  id='edit_button"+id+"' value='edit' onclick='edit_order("+id+");'/><input type='button' id='delete_button"+id+"' value='delete' onclick='delete_order("+id+");'/></td></tr>";
                  $('#inventory')[0].reset();
                  document.getElementById("result").innerHTML="";
                }     
            }
        });
    });
});

</script>
</html>