
<div style="overflow-x:auto;">
  <table style="width:100%" id="order_table">
  <caption>Orders</caption>
  <tr>
    <th>User</th>
    <th>Product</th>
    <th>Price</th>
    <th>Quantity</th>
    <th>Total</th>
    <th>Date</th>
    <th>Action</th>
  </tr>
 <?php if(!empty($orders)) { foreach ($orders as $row) { ?>
  <tr id="order<?php echo $row['id']; ?>">
  <td id="client<?php echo $row['id']; ?>"><?php echo $row["User"];?></td>
  <td id="product<?php echo $row['id']; ?>"><?php echo $row["Product"];?></td>
  <td id="price<?php echo $row['id']; ?>"><?php echo $row["Price"]." EUR";?></td>
  <td id="quantity<?php echo $row['id']; ?>"><?php echo $row["Quantity"];?></td>
  <td id="total<?php echo $row['id']; ?>"><?php echo $row["Total"]." EUR";?></td>
  <td id="date<?php echo $row['id']; ?>"><?php echo $row["Date"];?></td>
  <td>
   <input type='button'  id="edit_button<?php echo $row["id"];?>" value="edit" onclick="edit_order('<?php echo $row["id"];?>');"> 
   <input type='button'  id="delete_button<?php echo $row["id"];?>" value="delete" onclick="delete_order('<?php echo $row["id"];?>');">
  </td>
  </tr>
 <?php } } ?>
</table>
<br/>
<?php echo $this->ajax_pagination->create_links() ?>
 </div>
 