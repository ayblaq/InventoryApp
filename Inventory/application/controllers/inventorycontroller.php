<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
session_start();

class Inventorycontroller extends CI_Controller
 {

    public function __construct()
    {
        parent:: __construct();
        $this->load->library("Ajax_pagination");
        $this->load->model('inventory_model');
        $this->perpage = 5;
    }

    public function index() 
    {   
        $config = array();
        $data = [];     
        $config["base_url"] = base_url() . "inventorycontroller/paginated";
        $config["total_rows"] = $this->inventory_model->order_count();
        $config["per_page"] = $this->perpage;
        $config["uri_segment"] = 3;
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $config['link_func']   = 'searchFilter';
        $this->ajax_pagination->initialize($config);
        $page = $this->input->post('page');	
        $data['css'] = $this->load->view('shared/call_css', '', TRUE);
        $data['orders'] = $this->inventory_model->get_orders($config["per_page"], $page);    
        $data['clients'] = $this->inventory_model->get_clients();  
        $this->load->view('public/index',$data);
    
    }

    public function paginated()
    {
        $request = array();     
        $page = $this->input->post('page');        
        
        if(!$page){
            
            $offset = 0;
        } else{

            $offset = $page;        
        }
        
        $search = $this->input->post('search');       
        $filter = $this->input->post('filter');
        $request['filter'] = 0;

        if (!empty($search)) {
            
            $request['search'] = $search;        
        }
        
        if (!empty($filter)) {
            
            $request['filter'] = $filter;        
        }
          
        $count = count($this->inventory_model->getpaginated($request));        
        $config['target']      = '#history';
        $config['base_url']    = base_url().'inventorycontroller/paginated';        
        $config['total_rows']  = $count;        
        $config['per_page']    = $this->perpage;
        $config['link_func']   = 'searchFilter';
        $this->ajax_pagination->initialize($config);
        $request['start'] = $offset;
        $request['limit'] = $this->perpage;
        $data['orders'] = $this->inventory_model->getpaginated($request);
        $this->load->view('public/paginated', $data, false);
    
    }

    public function validate() 
    {
        $product  = $this->input->post('product');
        $quantity = $this->input->post('quantity');
        $client = $this->input->post('client');
        $id = $this->input->post('update');

        if (is_numeric($quantity)) {
            
            $price = $this->getprice($product);
            $total = $this->compute($quantity,$price,$product);
            $result = $this->inventory_model->add_order($client,$quantity,$product,$id,$total,$price);
            if ($result == -1) {            
                echo -1;                
            } else {               
               
                echo $result;
            }
        } else {          
            
            echo -2;
        }
        
    }

    public static function getprice($product) 
    {
        if ($product == "Fanta") {
            return 6.8;
        
        }elseif ($product == "Pepsi") {
            return 5.8;
        
        }elseif ($product == "Sprite") {
            return 3.8;
        
        }elseif ($product == "Lemon") {
            return 2.8;
        
        }else {
            return 4.8;
        }
    }

    public static function compute($quantity,$price,$product)
    {

        $result = $quantity * $price;
        if($product == "Pepsi") {
            if ($quantity >= 3) {

                $discount = 0.20 * $result ;
                $value = $result - $discount;
            }
            return $result;
        }

        return $result;
    }

    public function delete()
    {
        if (isset($_POST['delete_row'])) {
            $order = $_POST['row_id'];
            $result =  $this->inventory_model->delete_order($order);
            if ($result == 1) {             
                echo "success";
            } else {
                echo "failed";
            }
        } else {
            echo "failed";
        
        }
    }

}