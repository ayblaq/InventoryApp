A web application created for  Adcash full stack job position.
The application was created using code igniter MVC framework 
, php , html , ajax and java script.

A database was added to test the application.

How to use:

1. Add code_igniter folder to the same path of the InventoryApp (outside the application folder path)
2. Import inventorydb.sql 
3. load app at http://localhost/inventory/